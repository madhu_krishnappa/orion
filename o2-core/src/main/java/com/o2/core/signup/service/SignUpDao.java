package com.o2.core.signup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.o2.core.signup.dao.IsignUpService;
import com.o2.core.signup.model.SignUpResultVO;
import com.o2.core.signup.model.SignUpVO;

@Component
public class SignUpDao implements ISignupDao {
	
	@Autowired
	private IsignUpService signupDao;

	@Override
	public SignUpResultVO save(SignUpVO sigin) throws Exception {
		return signupDao.save(sigin);
	}

}
