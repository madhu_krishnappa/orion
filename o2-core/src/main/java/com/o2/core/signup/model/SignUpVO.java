package com.o2.core.signup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class SignUpVO {
	@Id
	@Column(name="id")
	private long id;
	@Column(name="user_name")
	private String emailId;
	@Column(name="user_password")
	private String password;
	
	@Column(name="first_name")
	private String firstName;
	
	
	
	@Override
	public String toString() {
		return "SignUpVO [emailId=" + emailId + ", password=" + password
				+ ", firstName=" + firstName + "]";
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
