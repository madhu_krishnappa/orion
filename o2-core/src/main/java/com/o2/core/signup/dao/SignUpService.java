package com.o2.core.signup.dao;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.o2.core.common.model.UserContext;
import com.o2.core.myclue.db.dao.MyclueDaoImpl;
import com.o2.core.signup.model.SignUpResultVO;
import com.o2.core.signup.model.SignUpVO;

@Component
public class SignUpService implements IsignUpService {
	
	static final Logger logger = Logger.getLogger(MyclueDaoImpl.class);

	@Autowired
	 private SessionFactory sessionFactory;

	 Session session = null;
	 Transaction tx = null;

	@Transactional
	public SignUpResultVO save(SignUpVO sigin) throws Exception
	{
		System.out.println("Inside dao impl");
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(sigin);
		tx.commit();
		session.close();
		return null;
		
	}


}
