package com.o2.core.signup.model;

import com.o2.core.common.model.GenericClueResponseVO;

public class SignUpResponseVO extends GenericClueResponseVO {
	public String getGeneratedUserId() {
		return generatedUserId;
	}

	public void setGeneratedUserId(String generatedUserId) {
		this.generatedUserId = generatedUserId;
	}

	private String generatedUserId;
}
