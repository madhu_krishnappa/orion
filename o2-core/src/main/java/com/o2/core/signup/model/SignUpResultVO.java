package com.o2.core.signup.model;

public class SignUpResultVO {
	public String getGeneratedUserId() {
		return generatedUserId;
	}

	public void setGeneratedUserId(String generatedUserId) {
		this.generatedUserId = generatedUserId;
	}

	private String generatedUserId;
}
