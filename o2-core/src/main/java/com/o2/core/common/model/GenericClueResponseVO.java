package com.o2.core.common.model;

import java.util.List;

import com.o2.core.myclue.db.entity.ClueData;

public class GenericClueResponseVO<T> {
	
	private ResponseHeader header;
	
	private List<ClueData> body;

	public ResponseHeader getHeader() {
		return header;
	}

	public void setHeader(ResponseHeader header) {
		this.header = header;
	}

	public List<ClueData> getBody() {
		return body;
	}

	public void setBody(List<ClueData> ans) {
		this.body = ans;
	}

	@Override
	public String toString() {
		return "GenericResponseVO [header=" + header + ", body=" + body + "]";
	}


	

}
