package com.o2.core.common.model;

import java.util.List;

import com.o2.core.myclue.db.entity.ClueData;
import com.o2.core.signup.model.SignUpVO;

public class GenericResponseVO<T> {
	
	private ResponseHeader header;
	
	private T body;

	public ResponseHeader getHeader() {
		return header;
	}

	public void setHeader(ResponseHeader header) {
		this.header = header;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T signUpVO) {
		this.body = signUpVO;
	}

	@Override
	public String toString() {
		return "GenericResponseVO [header=" + header + ", body=" + body + "]";
	}


	

}
