package com.o2.core.common.model;

import java.util.List;

import com.o2.core.myclue.db.entity.ClueData;

public class GenericClueRequestVO<T> {
	
	private RequestHeader header;
	
	private List<ClueData> body;

	public RequestHeader getHeader() {
		return header;
	}

	public void setHeader(RequestHeader header) {
		this.header = header;
	}

	public List<ClueData> getBody() {
		return body;
	}

	public void setBody(List<ClueData> ans) {
		this.body = ans;
	}

	@Override
	public String toString() {
		return "GenericRequestVO [header=" + header + ", body=" + body + "]";
	}
	
	
}
