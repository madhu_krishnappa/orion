package com.o2.core.common.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.o2.core.common.model.GenericClueRequestVO;
import com.o2.core.common.model.GenericClueResponseVO;
import com.o2.core.common.model.GenericRequestVO;
import com.o2.core.common.model.GenericResponseVO;
import com.o2.core.common.model.UserContext;
import com.o2.core.editclue.db.entity.UpdateClueVO;
import com.o2.core.editclue.handler.EditClueRESTHandler;
import com.o2.core.myclue.db.dao.ImyclueDao;
import com.o2.core.myclue.db.entity.AnsClueMap;
import com.o2.core.myclue.db.entity.ClueAns;
import com.o2.core.myclue.db.entity.ClueData;
import com.o2.core.myclue.handler.MyClueRESTHandler;
import com.o2.core.signup.handler.SignUpRESTHandler;
import com.o2.core.signup.model.SignUpResultVO;
import com.o2.core.signup.model.SignUpVO;

@Configuration

@Controller
public class O2Controller {

	@Autowired
	private SignUpRESTHandler signUpRequestHandler;
	
	@Autowired
	private MyClueRESTHandler myclueRESTHandler;
	
	@Autowired
	private ImyclueDao myclueService;

	@Autowired
	private EditClueRESTHandler editclueHandler;
	/**
	 * This method accepts incoming sign up requests.
	 * 
	 * @param signupRequestVO
	 * @return
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
	public @ResponseBody
	GenericResponseVO<SignUpResultVO> signUp(
			@RequestBody(required = false) GenericRequestVO<SignUpVO> signupRequestVO) {
		
		return signUpRequestHandler.handle(signupRequestVO);
	}

	private UserContext getUserContext() {
		return null;
	}

	/**
	 * This is a test method for testing connection.
	 * 
	 * @param inputField
	 * @return
	 */
	@RequestMapping(value = "/test", method = RequestMethod.GET, consumes = "application/json", produces = "application/json;charset=UTF-8")
	public @ResponseBody
	String test(@RequestParam("key1") String inputField) {
		return "ECHO " + inputField;
	}
	
	@RequestMapping(value = "/clueData", method = RequestMethod.GET)
	public @ResponseBody GenericClueResponseVO<ClueData> myclue(@RequestBody(required=false)GenericClueRequestVO<ClueData> cluedata  ) throws Exception{
	return myclueRESTHandler.handle(cluedata);
}
	
	@RequestMapping(value = "/clueAns", method = RequestMethod.GET)
	public @ResponseBody List<AnsClueMap> myclueAns( ) throws Exception{
		
		
		List<AnsClueMap> anscluemap=null;
		try{
			System.out.println("inside controller");
			anscluemap= myclueRESTHandler.handleClue(anscluemap);
		}
		catch (Exception e) {
			e.printStackTrace();
		
	}

	return anscluemap;
}
	
	@RequestMapping(value = "/updateClue", method = RequestMethod.PUT)
	public @ResponseBody void update(@RequestBody UpdateClueVO updateClueVO )throws Exception 
	{	
		
		System.out.println("inside controller");

		ClueData clue = new ClueData();
		clue.setId(updateClueVO.getClueId());
		clue.setClue(updateClueVO.getClue());
		clue.setClueDesc(updateClueVO.getClueDesc());
		clue.setClueLvl(updateClueVO.getClueLvl());
		
		try{
			editclueHandler.edit(clue);
			
		}
		catch (Exception e) {
			e.printStackTrace();
		
	}
		
		
		
		
	}
}