package com.o2.core.common.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.o2.core.myclue.db.entity.ClueAns;

@Entity
@Table(name = "country_code")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CountryCode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "cntry_Code")
	private long cntryCode;

	@Column(name = "cntry_Name")
	private String cntryName;
	
	@OneToMany(mappedBy="cntryCode")
	private Set<ClueAns> ansClue;
	
	
	
	public CountryCode(){}
	
	public long getCntryCode() {
		return cntryCode;
	}

	public void setId(long cntryCode) {
		this.cntryCode = cntryCode;
	}
	
	public String getCntryName() {
		return cntryName;
	}

	

	public void setCntryName(String cntryName) {
		this.cntryName = cntryName;
	}
}
