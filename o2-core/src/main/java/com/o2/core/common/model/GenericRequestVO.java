package com.o2.core.common.model;

import java.util.List;

import com.o2.core.myclue.db.entity.ClueData;

public class GenericRequestVO<T> {
	
	private RequestHeader header;
	
	private T body;

	public RequestHeader getHeader() {
		return header;
	}

	public void setHeader(RequestHeader header) {
		this.header = header;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T ans) {
		this.body = ans;
	}

	@Override
	public String toString() {
		return "GenericRequestVO [header=" + header + ", body=" + body + "]";
	}
	
	
}
