package com.o2.core.editclue.db.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.o2.core.myclue.db.dao.MyclueDaoImpl;
import com.o2.core.myclue.db.entity.ClueData;

public class EditclueDaoImpl implements IeditclueDao {
	
	static final Logger logger = Logger.getLogger(MyclueDaoImpl.class);

	@Autowired
	 private SessionFactory sessionFactory;

	 Session session = null;
	 Transaction tx = null;

	@Override
	public void update(ClueData clue) throws Exception {
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.update(clue);	
		tx.commit();
		session.close();
		
	}

}
