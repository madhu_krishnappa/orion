package com.o2.core.editclue.db.entity;

import javax.persistence.Id;

public class UpdateClueVO {
	
	@Id
	private long clueId;
	private String clue;
	private String clueDesc;
	private int clueLvl;
	
	public long getClueId() {
		return clueId;
	}
	public void setClueId(long clueId) {
		this.clueId = clueId;
	}
	
	
	public String getClue() {
		return clue;
	}
	public void setClue(String clue) {
		this.clue = clue;
	}
	public String getClueDesc() {
		return clueDesc;
	}
	public void setClueDesc(String clueDesc) {
		this.clueDesc = clueDesc;
	}
	public int getClueLvl() {
		return clueLvl;
	}
	public void setClueLvl(int clueLvl) {
		this.clueLvl = clueLvl;
	}
	
	

}
