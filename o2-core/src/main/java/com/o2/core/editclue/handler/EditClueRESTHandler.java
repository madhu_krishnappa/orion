package com.o2.core.editclue.handler;

import org.springframework.beans.factory.annotation.Autowired;

import com.o2.core.editclue.db.dao.EditclueDaoImpl;
import com.o2.core.myclue.db.entity.ClueData;

public class EditClueRESTHandler {
	
	@Autowired
	private EditclueDaoImpl editService;

	public ClueData edit(ClueData clue) {
		  try {
			editService.update(clue);
		} catch (Exception e) {
			e.printStackTrace();
		};
		return clue;
		
		
	}

}
