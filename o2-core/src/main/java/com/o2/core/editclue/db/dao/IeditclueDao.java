package com.o2.core.editclue.db.dao;

import com.o2.core.myclue.db.entity.ClueData;


public interface IeditclueDao {

	public void update(ClueData clue)throws Exception;

}
