package com.o2.core.myclue.db.dao;

import java.util.List;












import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.o2.core.common.model.GenericClueRequestVO;
import com.o2.core.common.model.GenericClueResponseVO;
import com.o2.core.myclue.db.entity.AnsClueMap;
import com.o2.core.myclue.db.entity.ClueData;

@Component
public class MyclueDaoImpl implements ImyclueDao{
	static final Logger logger = Logger.getLogger(MyclueDaoImpl.class);

	@Autowired
	 private SessionFactory sessionFactory;

	 Session session = null;
	 Transaction tx = null;
	 
	
	
	



	@Override
	public List<ClueData> getClueData() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<ClueData> ansClueList =  session.createCriteria(ClueData.class).list();
		tx.commit();
		session.close();
		return ansClueList;
	}







	@Override
	public List<AnsClueMap> getClueAns() {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<AnsClueMap> ansClueMap = session.createCriteria(AnsClueMap.class).list();
		tx.commit();
		session.close();
		return ansClueMap;
	}


}