package com.o2.core.myclue.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.o2.core.common.model.GenericClueRequestVO;
import com.o2.core.common.model.GenericClueResponseVO;
import com.o2.core.common.model.ResponseHeader;
import com.o2.core.common.model.Status;
import com.o2.core.myclue.db.dao.ImyclueDao;
import com.o2.core.myclue.db.dao.MyclueDaoImpl;
import com.o2.core.myclue.db.entity.AnsClueMap;
import com.o2.core.myclue.db.entity.ClueData;


@Component
public class MyClueRESTHandler {
	
	@Autowired
	private MyclueDaoImpl myclueService;
	
	

	public GenericClueResponseVO<ClueData> handle(GenericClueRequestVO<ClueData> clue) throws Exception {
		
		GenericClueResponseVO<ClueData> anscluedata=new GenericClueResponseVO<ClueData>();
		List<ClueData> ans=myclueService.getClueData();
		
		anscluedata.setBody(ans);
		
		ResponseHeader header=new ResponseHeader();
		if(ans.isEmpty()){
			header.setStatus(new Status("hi","1","welcome"));
		}else
		{header.setStatus(new Status("hi","1","bye"));
		}
		
		anscluedata.setHeader(header);
		return anscluedata;
	}
	
	public List<AnsClueMap> handleClue(List<AnsClueMap> anscluemap) throws Exception {
		anscluemap= myclueService.getClueAns();
		return anscluemap;
	}

	

}
