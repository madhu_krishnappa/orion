package com.o2.core.myclue.db.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "answer_clue_mapping")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AnsClueMap implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Clue_Ans_ID")
	private long Clue_Ans_ID;
	
	
	@ManyToOne
	@JoinColumn(name="CLUE_DATA_Clue_ID", nullable=false)
	private ClueData clueData;
	
	
	@ManyToOne
	@JoinColumn(name="CLUE_ANSWER_Ans_ID", nullable=false)
	private ClueAns clueAns;
	
	
	
	

	public AnsClueMap() {
	}
		
	public AnsClueMap(ClueData clueData, ClueAns clueAns) {
		super();
		this.clueData = clueData;
		this.clueAns = clueAns;
	}

	
	
	public long getClue_Ans_ID() {
		return Clue_Ans_ID;
	}

	public void setClue_Ans_ID(long clue_Ans_ID) {
		Clue_Ans_ID = clue_Ans_ID;
	}

	public ClueAns getClueAns() {
		return clueAns;
	}

	public void setClueAns(ClueAns clueAns) {
		this.clueAns = clueAns;
	}

	public ClueData getClueData() {
		return clueData;
	}

	public void setClueData(ClueData clueData) {
		this.clueData = clueData;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
