package com.o2.core.myclue.db.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.security.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "clue_data",uniqueConstraints={@UniqueConstraint(columnNames={"clue_Id"})})
public class ClueData implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "clue_Id", nullable=false, unique=true, length=11)
	private long clueId;

	@Column(name = "clue")
	private String clue;
	
	@Column(name = "clue_Desc")
	private String clueDesc;
	
	@Column(name = "clue_Lvl")
	private long clueLvl;
	
	@Column(name = "user_Id")
	private long userId;
	
	@Column(name = "clue_Validation_Rule_Rule_Id")
	private long clueValidationRuleRuleId;
	
	@Column(name = "clue_Trans_Id")
	private long clueTransId;
	
	@Column(name = "CD_CREATED_TS")
	private Date cdCreatedTs;
	
	
	@OneToMany(mappedBy="clueData") 
	private Set<AnsClueMap> ansClue;
	
	
	
	public ClueData(){}
		
	public long getClueId() {
		return clueId;
	}

	public void setId(long clueId) {
		this.clueId = clueId;
	}

	public String getClue() {
		return clue;
	}

	public void setClue(String clue) {
		this.clue = clue;
	}
	
	public String getClueDesc() {
		return clueDesc;
	}

	public void setClueDesc(String clueDesc) {
		this.clueDesc = clueDesc;
	}
	
	public long getClueLvl() {
		return clueLvl;
	}

	public void setClueLvl(long l) {
		this.clueLvl = l;
	}
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getClueValidationRuleRuleId() {
		return clueValidationRuleRuleId;
	}

	public void setClueValidationRuleRuleId(long clueValidationRuleRuleId) {
		this.clueValidationRuleRuleId = clueValidationRuleRuleId;
	}
	
	public long getClueTransId() {
		return clueTransId;
	}

	public void setClueTransId(long clueTransId) {
		this.clueTransId = clueTransId;
	}
	
	public Date getCdCreatedTs() {
		return cdCreatedTs;
	}
	
	public void setCdCreatedTs(Date cdCreatedTs) {
		this.cdCreatedTs = cdCreatedTs;
	}

}



