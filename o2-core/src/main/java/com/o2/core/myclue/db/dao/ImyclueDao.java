package com.o2.core.myclue.db.dao;

import java.util.List;

import com.o2.core.common.model.GenericClueRequestVO;
import com.o2.core.common.model.GenericClueResponseVO;
import com.o2.core.myclue.db.entity.AnsClueMap;
import com.o2.core.myclue.db.entity.ClueData;

public interface ImyclueDao {
	
	public List<ClueData> getClueData() throws Exception;
    public List<AnsClueMap> getClueAns();
}
